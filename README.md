To test, run `docker build -t myopensusedevel .`

Then can for example configure a project:
```
docker run --volume $HOME/projects/ark:/source/ark --volume $HOME/projects/build/ark:/build/ark -w /build/ark os cmake /source/ark
```

