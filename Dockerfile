FROM opensuse:tumbleweed

RUN zypper -n addrepo --priority 50 --refresh obs://KDE:Unstable:Qt/openSUSE_Tumbleweed KDE:Unstable:Qt
RUN zypper -n --gpg-auto-import-keys -v dup
RUN zypper -n in --recommends patterns-kde-devel_kde_frameworks
RUN zypper -n in clang

